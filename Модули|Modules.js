Модули = function(имя)
{
    this.выбралиЭлемент = new Уведомитель();
    this.выбранныйЭлемент = null;

    this._создать = function()
    {
        var html = `
<div id="${имя}" class="uk-container uk-margin-top uk-margin-bottom">
    <ul id="${имя}-список">
    </ul>
</div>
        `;
        var css = `
#${имя}-список
{
    list-style-type: none;
    padding-left: 0px;
}
        `;

        $("body").append(html);
        // Вид.
        var вид = document.createElement("style");
        вид.innerHTML = css;
        document.head.appendChild(вид);
    };
    
    this.добавить = function(указатель, название)
    {
        var html = `
<li id="${название}" class="uk-margin">
    <div class="uk-card uk-card-default uk-card-body uk-card-hover">
        ${название}
    </div>
</li>
        `;
        $(`#${имя}-список`).append(html);
        
        var тут = this;
        $(`#${название}`).click(function() {
            тут.выбранныйЭлемент = указатель;
            тут.выбралиЭлемент.уведомить();
        });
    };
    
    this.очистить = function()
    {
        $(`#${имя}-список`).html("");
    };

    /*
    this.задать = function(элементы)
    {
        this.очистить();
        for (var номер in элементы)
        {
            var эл = элементы[номер];
            this.добавить(эл);
        }
    };
    */

    this._создать(имя);
};
